import org.example.CalculadoraCasio;
import org.example.CalculadoraChina;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculadoraTest {

    // Tests calculadora Casio

    @Test
    public void testSumaPositivaCasio(){
        CalculadoraCasio calculadoraCasio = new CalculadoraCasio();
        Integer resultadoCasio = calculadoraCasio.sumar(1,1);
        Assertions.assertEquals(2, resultadoCasio);
    }

    @Test
    public void testSumaNegativaCasio(){
        CalculadoraCasio calculadoraCasio = new CalculadoraCasio();
        Integer resultadoCasio = calculadoraCasio.sumar(1,-10);
        Assertions.assertEquals(-9, resultadoCasio);
    }

    @Test
    public void testRestaPositivaCasio(){
        CalculadoraCasio calculadoraCasio = new CalculadoraCasio();
        Integer resultadoCasio = calculadoraCasio.restar(10,1);
        Assertions.assertEquals(9, resultadoCasio);
    }

    @Test
    public void testRestaNegativaCasio(){
        CalculadoraCasio calculadoraCasio = new CalculadoraCasio();
        Integer resultadoCasio = calculadoraCasio.restar(0,1);
        Assertions.assertEquals(-1, resultadoCasio);
    }

    @Test
    public void testMultiplicacionPositvaCasio(){
        CalculadoraCasio calculadoraCasio = new CalculadoraCasio();
        Integer resultadoCasio = calculadoraCasio.multipicar(6,4);
        Assertions.assertEquals(24, resultadoCasio);
    }

    @Test
    public void testMultiplicacionCeroCasio(){
        CalculadoraCasio calculadoraCasio = new CalculadoraCasio();
        Integer resultadoCasio = calculadoraCasio.multipicar(666,0);
        Assertions.assertEquals(0, resultadoCasio);
    }

    @Test
    public void testMultiplicacionUnNegativoCasio(){
        CalculadoraCasio calculadoraCasio = new CalculadoraCasio();
        Integer resultadoCasio = calculadoraCasio.multipicar(-7,3);
        Assertions.assertEquals(-21, resultadoCasio);
    }

    @Test
    public void testMultiplicacionDobleNegativoCasio(){
        CalculadoraCasio calculadoraCasio = new CalculadoraCasio();
        Integer resultadoCasio = calculadoraCasio.multipicar(-3,-5);
        Assertions.assertEquals(15, resultadoCasio);
    }

    @Test
    public void testDivisionPositivaCasio(){
        CalculadoraCasio calculadoraCasio = new CalculadoraCasio();
        Double resultadoCasio = calculadoraCasio.dividir(12.0,2.0);
        Assertions.assertEquals(6.0, resultadoCasio);
    }

    @Test
    public void testDivisionCeroCasio(){
        CalculadoraCasio calculadoraCasio = new CalculadoraCasio();
        Double resultadoCasio = calculadoraCasio.dividir(5.0,0.0);
        Assertions.assertEquals(Double.POSITIVE_INFINITY, resultadoCasio);
    }

    @Test
    public void testDivisionUnNegativoCasio(){
        CalculadoraCasio calculadoraCasio = new CalculadoraCasio();
        Double resultadoCasio = calculadoraCasio.dividir(-12.0,2.0);
        Assertions.assertEquals(-6.0, resultadoCasio);
    }

    @Test
    public void testDivisionDobleNegativoCasio(){
        CalculadoraCasio calculadoraCasio = new CalculadoraCasio();
        Double resultadoCasio = calculadoraCasio.dividir(-12.0,-2.0);
        Assertions.assertEquals(6.0, resultadoCasio);
    }

    // Tests calculadora china

    @Test
    public void testSumaPositivaChina(){
        CalculadoraChina calculadoraChina = new CalculadoraChina();
        Integer resultadoChina = calculadoraChina.sumar(6,9);
        Assertions.assertEquals(15, resultadoChina);
    }

    @Test
    public void testSumaNegativaChina(){
        CalculadoraChina calculadoraChina = new CalculadoraChina();
        Integer resultadoChina = calculadoraChina.sumar(5,-20);
        Assertions.assertEquals(-15, resultadoChina);
    }

    @Test
    public void testRestaPositivaChina(){
        CalculadoraChina calculadoraChina = new CalculadoraChina();
        Integer resultadoChina = calculadoraChina.restar(9,7);
        Assertions.assertEquals(2, resultadoChina);
    }

    @Test
    public void testRestaNegativaChina(){
        CalculadoraChina calculadoraChina = new CalculadoraChina();
        Integer resultadoChina = calculadoraChina.restar(3,9);
        Assertions.assertEquals(-6, resultadoChina);
    }

    @Test
    public void testMultiplicacionPositvaChina(){
        CalculadoraChina calculadoraChina = new CalculadoraChina();
        Integer resultadoChina = calculadoraChina.multipicar(3,3);
        Assertions.assertEquals(9, resultadoChina);
    }

    @Test
    public void testMultiplicacionCeroChina(){
        CalculadoraChina calculadoraChina = new CalculadoraChina();
        Integer resultadoChina = calculadoraChina.multipicar(0,0);
        Assertions.assertEquals(0, resultadoChina);
    }

    @Test
    public void testMultiplicacionUnNegativoChina(){
        CalculadoraChina calculadoraChina = new CalculadoraChina();
        Integer resultadoChina = calculadoraChina.multipicar(9,-5);
        Assertions.assertEquals(-45, resultadoChina);
    }

    @Test
    public void testMultiplicacionDobleNegativoChina(){
        CalculadoraChina calculadoraChina = new CalculadoraChina();
        Integer resultadoChina = calculadoraChina.multipicar(-5,-5);
        Assertions.assertEquals(25, resultadoChina);
    }

    @Test
    public void testDivisionPositivaChina(){
        CalculadoraChina calculadoraChina = new CalculadoraChina();
        Double resultadoChina = calculadoraChina.dividir(9.0,3.0);
        Assertions.assertEquals(3.0, resultadoChina);
    }

    @Test
    public void testDivisionCeroChina(){
        CalculadoraChina calculadoraChina = new CalculadoraChina();
        Double resultadoChina = calculadoraChina.dividir(5.0,0.0);
        Assertions.assertEquals(Double.POSITIVE_INFINITY, resultadoChina);
    }

    @Test
    public void testDivisionUnNegativoChina(){
        CalculadoraChina calculadoraChina = new CalculadoraChina();
        Double resultadoChina = calculadoraChina.dividir(2.0,-1.0);
        Assertions.assertEquals(-2.0, resultadoChina);
    }

    @Test
    public void testDivisionDobleNegativoChina(){
        CalculadoraChina calculadoraChina = new CalculadoraChina();
        Double resultadoChina = calculadoraChina.dividir(-25.0,-5.0);
        Assertions.assertEquals(5, resultadoChina);
    }

}
