package org.example;

import java.util.ArrayList;
import java.util.List;

public class CalculadoraCasio implements Calculadora{

    String marca;
    String serial;
    String modelo;
    List<String> operaciones = new ArrayList<>();

    @Override
    public Integer sumar(Integer numeroUno, Integer numeroDos) {
        int resultado = numeroUno + numeroDos;
        this.operaciones.add(numeroUno + " + " + numeroDos + " = " + resultado);
        return resultado;
    }

    @Override
    public Integer restar(Integer numeroUno, Integer numeroDos) {
        int resultado = numeroUno - numeroDos;
        this.operaciones.add(numeroUno + " - " + numeroDos + " = " + resultado);
        return resultado;
    }

    @Override
    public Integer multipicar(Integer numeroUno, Integer numeroDos) {
        int resultado = numeroUno * numeroDos;
        this.operaciones.add(numeroUno + " * " + numeroDos + " = " + resultado);
        return resultado;
    }

    @Override
    public Double dividir(Double numeroUno, Double numeroDos) {
        double resultado = numeroUno / numeroDos;
        this.operaciones.add(numeroUno + " / " + numeroDos + " = " + resultado);
        return resultado;

    }

}
