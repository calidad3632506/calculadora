package org.example;

public class CalculadoraChina implements Calculadora {

    String serial;

    public Integer sumar(Integer numeroUno, Integer numeroDos){
        int resultado = numeroUno + numeroDos;
        return resultado;
    }

    public Integer restar(Integer numeroUno, Integer numeroDos){
        int resultado = numeroUno - numeroDos;
        return resultado;
    }

    public Integer multipicar(Integer numeroUno, Integer numeroDos){
        int resultado = numeroUno * numeroDos;
        return resultado;
    }

    public Double dividir(Double numeroUno, Double numeroDos){
        double resultado = numeroUno / numeroDos;
        return resultado;
    }

}
