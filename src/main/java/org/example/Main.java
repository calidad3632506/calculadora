package org.example;

public class Main {

    public static void main(String[] args) {

        CalculadoraCasio calculadoraCasio = new CalculadoraCasio();
        calculadoraCasio.marca = "Cassio";
        calculadoraCasio.serial = "10293";
        calculadoraCasio.modelo = "K210";

        Integer resultadoSumaCasio = calculadoraCasio.sumar(1,2);

        System.out.println("La suma da como resultado: " + resultadoSumaCasio );

        Integer resultadoRestaCasio = calculadoraCasio.restar(5,1);

        System.out.println("La resta da como resultado: " + resultadoRestaCasio);

        Integer resultadoMultiplicacionCasio = calculadoraCasio.multipicar(6,4);

        System.out.println("La multiplicacion da como resultado: " + resultadoMultiplicacionCasio);

        Double resultadoDivisionCasio = calculadoraCasio.dividir(2.0,2.0);

        System.out.println("La division da como resultado: " + resultadoDivisionCasio);


        CalculadoraChina calculadoraChina = new CalculadoraChina();
        calculadoraChina.serial = "123";

        Integer resultadoSumaChina = calculadoraChina.sumar(2,1);

        System.out.println("La suma da como resultado: " + resultadoSumaChina );

        Integer resultadoRestaChina = calculadoraChina.restar(1,5);

        System.out.println("La resta da como resultado: " + resultadoRestaChina);

        Integer resultadoMultiplicacionChina = calculadoraChina.multipicar(4,6);

        System.out.println("La multiplicacion da como resultado: " + resultadoMultiplicacionChina);

        Double resultadoDivisionChina = calculadoraChina.dividir(2.0,2.0);

        System.out.println("La division da como resultado: " + resultadoDivisionChina);

    }

}
