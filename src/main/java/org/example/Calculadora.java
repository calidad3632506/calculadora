package org.example;

public interface Calculadora {

    Integer sumar(Integer numeroUno, Integer numeroDos);

    Integer restar(Integer numeroUno, Integer numeroDos);

    Integer multipicar(Integer numeroUno, Integer numeroDos);

    Double dividir(Double numeroUno, Double numeroDos);

}
